# # rank_mm vs hoepman's algorithm
import networkx as nx, simpy, hoepman as hp, rank_mm as rmm

node_sizes = [20, 40, 60, 80]
number_of_tests = 5
results = []

# hoepmans_alg_avg_runtimes = list()
# hoepmans_alg_avg_runtime = list()

hoepmans_alg_message_types = ['req', 'drop']
rank_mm_alg_message_types = ['ROUND', 'propose', 'unpropose', 'neigh_ack', 'ack', 'reject', 'inactive', 'neigh_rej']

for node_size in node_sizes:
    hoepmans_alg_avg_runtime = 0
    hoepmans_alg_avg_message_counter = 0
    rank_mm_alg_avg_runtime = 0
    rank_mm_alg_avg_message_counter = 0
    for test_number in range(0, number_of_tests):
        print("===== Test number: %d, node size: %d =====" % (test_number + 1, node_size))

        my_nx_graph = nx.connected_watts_strogatz_graph(node_size, 4, 0.3)

        env_hoepmans = simpy.Environment()
        hoepmans_graph = hp.Graph(env=env_hoepmans, nodeObject=hp.DNode)
        hoepmans_graph.setSystem('ASYNC')
        hoepmans_graph.load_graph_from_nx(my_nx_graph)
        env_hoepmans.run(node_size * 1)

        max_last_msg_time_in_graph = 0
        for node in hoepmans_graph.nodes:
            if node.lastMsgTime > max_last_msg_time_in_graph:
                max_last_msg_time_in_graph = node.lastMsgTime + 1

            for message_type in hoepmans_alg_message_types:
                if message_type in node.messageCounter:
                    hoepmans_alg_avg_message_counter += float(node.messageCounter[message_type]) / number_of_tests

        hoepmans_alg_avg_runtime += float(max_last_msg_time_in_graph) / number_of_tests


        env_rank_mm = simpy.Environment()
        rank_mm_graph = hp.Graph(env=env_rank_mm, nodeObject=rmm.DNode)
        rank_mm_graph.setSystem('SYNC')
        rank_mm_graph.load_graph_from_nx(my_nx_graph)
        env_rank_mm.run(node_size * 5)

        max_last_msg_time_in_graph = 0
        for node in rank_mm_graph.nodes:
            if node.lastMsgTime > max_last_msg_time_in_graph:
                max_last_msg_time_in_graph = node.lastMsgTime + 1

            for message_type in rank_mm_alg_message_types:
                print node.messageCounter
                if message_type in node.messageCounter:
                    rank_mm_alg_avg_message_counter += float(node.messageCounter[message_type]) / number_of_tests

        rank_mm_alg_avg_runtime += float(max_last_msg_time_in_graph) / number_of_tests


    results.append({'algorithm': 'hoepmans', 'node_size': node_size,
                    'avg_runtime': hoepmans_alg_avg_runtime, 'avg_message_count': hoepmans_alg_avg_message_counter})
    results.append({'algorithm': 'rank_mm', 'node_size': node_size,
                    'avg_runtime': rank_mm_alg_avg_runtime, 'avg_message_count': rank_mm_alg_avg_message_counter})

print("#Algorithm\t#Node Count\t\t#Runtime\t#Msg Count\t")
for result in results:
    print("%s\t\t%d\t\t\t\t%.1f\t\t%.1f" % (result['algorithm'] + " ", result['node_size'], result['avg_runtime'], result['avg_message_count']))
