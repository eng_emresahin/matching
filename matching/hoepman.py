from distSimulator import *


class DNode(Node):
    """ Constructor """

    def __init__(self, *args):
        Node.__init__(self, *args)
        self.S = set()
        self.R = set()
        self.matched_node_id = None

    """ This method describes what a node does"""

    def run(self):
        self.R = set()
        self.S = set(self.neighbor.keys())
        c = self.candidate()
        if c is not None:
            self.send_message(c, {'type': 'req'})
            print("Node %d sends %s message to Node %d at time: %d" % (self.id, 'req', c, self.env.now))
        while len(self.S) != 0:
            yield self.mailbox.get(1)
            [s, r, msg] = self.receive_message()
            if msg['type'] == 'req':
                self.R.add(s)
            elif msg['type'] == 'drop':
                self.S.remove(s)
                if c == s:
                    c = self.candidate()
                    if c is not None:
                        self.matched_node_id = c
                        self.send_message(c, {'type': 'req'})
                        print("Node %d sends %s message to Node %d at time: %d" % (self.id, 'req', c, self.env.now))
            print("=" * 50)
            print("After Node %d received %s\n S: %s, \t R: %s" % (self.id, msg['type'], self.S, self.R))
            print("=" * 50)
            if c is not None and c in self.R:
                for w in self.S.difference({c}):
                    self.matched_node_id = c
                    self.send_message(w, {'type': 'drop'})
                    print("Node %d sends %s message to Node %d at time: %d" % (self.id, 'drop', w, self.env.now))
                self.S.clear()

    def candidate(self):
        heaviest_node = None
        heaviest_weight = 0
        for i in self.S:
            if self.neighbor[i][1] > heaviest_weight:
                heaviest_weight = self.neighbor[i][1]
                heaviest_node = i
        return heaviest_node

#
# env = simpy.Environment()
# g = Graph(env, nodeCount=6, nodeObject=DNode)  #
# g.setSystem('ASYNC')  # 5 zaman biriminde bir tum node'lara ROUND mesaji gonderilir
#
# # Topolojiyi su sekilde olusturuyoruz
# g.add_edge(0, 5, 5)
# g.add_edge(4, 5, 6)
# g.add_edge(1, 5, 4)
# g.add_edge(1, 4, 1)
# g.add_edge(0, 1, 7)
# g.add_edge(4, 3, 9)
# g.add_edge(4, 2, 2)
# g.add_edge(3, 2, 3)
# g.add_edge(1, 2, 8)
#
# # Simulatoru baslatmak icin:
# env.run(50)
#
# # Algoritma sonlandiktan sonra node'larin bilgilerini su sekilde okuyabilirsiniz
# print("...\n...\n...\n  --- Status of nodes after termination --- ")
# for i in g.nodes:
#     print("Node: %d, R: %s" % (i.id, str(i.R)))
#
# printed = list()
# print("...\n...\n...\n  --- Status of matched nodes after termination ---")
# for i in g.nodes:
#     if i.matched_node_id is not None and (i.matched_node_id, i.id) not in printed:
#         print("Node %d matched with Node %d" % (i.id, i.matched_node_id))
#         printed.append((i.id, i.matched_node_id))

#
# #
# # matched = list()
# # for i in g.nodes:
# #     for j in g.nodes:
# #         if i.id != j.id:
# #             if i.R.intersection([j.id]) == set([j.id]) and j.R.intersection([i.id]) == set([i.id]):
# #                 if (i.id, j.id) not in matched and (j.id, i.id) not in matched:
# #                     matched.append((i.id, j.id))
# #
# # for i in matched:
# #     print("Node %d matched with Node %d" % (i[0], i[1]))
