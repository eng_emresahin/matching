from distSimulator import *
import random


class DNode(Node):

    def __init__(self, *args):
        Node.__init__(self, *args)
        self.matched = False
        self.received = set()
        self.curr_neighs = set()
        self.prop_flag = False
        self.round_recvd = False
        self.recvd_prop = set()
        self.reply_flag = False
        self.lost_neighs = set()
        self.matched_node_id = None

    """ This method describes what a node does"""

    def run(self):
        while True:
            yield self.mailbox.get(1)
            [s, r, msg] = self.receive_message()
            if msg['type'] == 'ROUND':
                if msg['round'] == 1:
                    self.curr_neighs = set(self.neighbor.keys())
                print("-" * 50)
                print("Node %d receives %s(%d) message" % (r, msg['type'], msg['round']))
                print("-" * 50)
            else:
                print("Node %d receives %s(%d) message from Node %d" % (r, msg['type'], msg['round'], s))
                print("Node %d, matched: %s, curr_neighs: %s, prop_flag: %s, round_recvd: %s, recvd_prop: %s, reply_flag: %s, lost_neighs: %s, received: %s"
                      % (self.id, str(self.matched), str(self.curr_neighs), str(self.prop_flag), str(self.round_recvd), str(self.recvd_prop), str(self.reply_flag), str(self.lost_neighs), str(self.received)))

            if msg['round'] % 2 == 1:
                if msg['type'] == 'ROUND':
                    if not self.matched and len(self.curr_neighs) != 0:
                        if self.id > max(self.curr_neighs):
                            j = random.choice(list(self.curr_neighs))
                            self.send_message(j, {'type': 'propose', 'round': msg['round']})
                            print("Node %d sends %s(%d) message to Node %d" % (self.id, 'propose', msg['round'], j))
                            for i in list(self.curr_neighs.difference({j})):
                                self.send_message(i, {'type': 'unpropose', 'round': msg['round']})
                                print("Node %d sends %s(%d) message to Node %d" % (self.id, 'unpropose', msg['round'], i))
                            self.prop_flag = True
                        else:
                            for j in list(self.curr_neighs):
                                self.send_message(j, {'type': 'unpropose', 'round': msg['round']})
                                print("Node %d sends %s(%d) message to Node %d" % (self.id, 'unpropose', msg['round'], j))
                    self.round_recvd = True
                elif msg['type'] == 'propose':
                    self.recvd_prop.add(s)
                    self.reply_flag = True
                elif msg['type'] == 'unpropose':
                    self.received.add(s)

                if self.round_recvd and (self.received.union(self.recvd_prop) == self.curr_neighs):
                    self.round_recvd = False
                    self.received.clear()

                print("After received %s message, \nNode %d, matched: %s, curr_neighs: %s, prop_flag: %s, round_recvd: %s, recvd_prop: %s, reply_flag: %s, lost_neighs: %s, received: %s"
                      % (msg['type'], self.id, str(self.matched), str(self.curr_neighs), str(self.prop_flag), str(self.round_recvd), str(self.recvd_prop), str(self.reply_flag), str(self.lost_neighs), str(self.received)))
            else:
                if msg['type'] == 'ROUND':
                    if self.reply_flag:
                        j = max(self.recvd_prop)
                        self.send_message(j, {'type': 'ack', 'round': msg['round']})
                        print("Node %d sends %s(%d) message to Node %d" % (self.id, 'ack', msg['round'], j))
                        for i in list(self.recvd_prop.difference({j})):
                            self.send_message(i, {'type': 'reject', 'round': msg['round']})
                            print("Node %d sends %s(%d) message to Node %d" % (self.id, 'reject', msg['round'], i))
                        # for k in list(self.curr_neighs.difference(self.recvd_prop.union({j}))):
                        for k in list(self.curr_neighs.difference(self.recvd_prop)):
                            self.send_message(k, {'type': 'inactive', 'round': msg['round']})
                            print("Node %d sends %s(%d) message to Node %d" % (self.id, 'inactive', msg['round'], k))
                        self.matched = True
                        self.matched_node_id = j
                    elif not self.prop_flag:
                        for j in list(self.curr_neighs):
                            self.send_message(j, {'type': 'inactive', 'round': msg['round']})
                            print("Node %d sends %s(%d) message to Node %d" % (self.id, 'inactive', msg['round'], j))
                    self.round_recvd = True
                elif msg['type'] == 'ack':
                    self.matched = True
                    self.matched_node_id = s
                    for j in list(self.curr_neighs):
                    # for j in list(self.curr_neighs.difference({s})):
                        self.send_message(j, {'type': 'neigh_ack', 'round': msg['round']})
                        print("Node %d sends %s(%d) message to Node %d" % (self.id, 'neigh_ack', msg['round'], j))
                elif msg['type'] == 'reject':
                    for k in list(self.curr_neighs):
                    # for k in list(self.curr_neighs.difference({s})):
                        self.send_message(k, {'type': 'neigh_rej', 'round': msg['round']})
                        print("Node %d sends %s(%d) message to Node %d" % (self.id, 'neigh_rej', msg['round'], k))
                elif msg['type'] == 'neigh_ack':
                    # print("Node %d received %s(%d) message from Node %d" % (r, msg['type'], msg['round'], s))
                    # print("Node %d, matched: %s, curr_neighs: %s, prop_flag: %s, round_recvd: %s, recvd_prop: %s, reply_flag: %s, lost_neighs: %s, received: %s"
                    #       % (self.id, str(self.matched), str(self.curr_neighs), str(self.prop_flag), str(self.round_recvd), str(self.recvd_prop), str(self.reply_flag), str(self.lost_neighs), str(self.received)))
                    self.lost_neighs.add(s)
                elif msg['type'] == 'neigh_rej' or msg['type'] == 'inactive':
                    print("Node %d received %s(%d) message from Node %d (just received)" % (r, msg['type'], msg['round'], s))

                if msg['type'] != 'ROUND':
                    self.received.add(s)

                if self.round_recvd and (self.received == self.curr_neighs):
                    self.prop_flag = False
                    self.reply_flag = False
                    self.curr_neighs = self.curr_neighs.difference(self.lost_neighs)
                    self.round_recvd = False
                    self.received.clear()
                print("After received %s message, \nNode %d, matched: %s, curr_neighs: %s, prop_flag: %s, round_recvd: %s, recvd_prop: %s, reply_flag: %s, lost_neighs: %s, received: %s"
                      % (msg['type'], self.id, str(self.matched), str(self.curr_neighs), str(self.prop_flag), str(self.round_recvd), str(self.recvd_prop), str(self.reply_flag), str(self.lost_neighs), str(self.received)))


env = simpy.Environment()
g = Graph(env, nodeCount=8, nodeObject=DNode)
g.setSystem('SYNC', 6)

# Topolojiyi su sekilde olusturuyoruz
g.add_edge(2, 5)
g.add_edge(2, 7)
g.add_edge(2, 4)
g.add_edge(0, 4)
g.add_edge(4, 5)
g.add_edge(4, 1)
g.add_edge(5, 0)
g.add_edge(0, 1)
g.add_edge(3, 0)
g.add_edge(3, 1)
g.add_edge(1, 6)

# Simulatoru baslatmak icin:
env.run(48)

# Algoritma sonlandiktan sonra node'larin bilgilerini su sekilde okuyabilirsiniz
print("...\n...\n...\n  --- Status of nodes after termination --- ")
for i in g.nodes:
    print("Node %d: matched: %s, curr_neighs: %s, lost_neighs: %s, recvd_prop: %s" % (i.id, str(i.matched), str(i.curr_neighs), str(i.lost_neighs), str(i.recvd_prop)))

printed = list()
print("...\n...\n...\n  --- Status of matched nodes after termination ---")
for i in g.nodes:
    if i.matched and (i.matched_node_id, i.id) not in printed:
        print("Node %d matched with Node %d" % (i.id, i.matched_node_id))
        printed.append((i.id, i.matched_node_id))
